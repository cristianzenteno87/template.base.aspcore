﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Telemetry.AppInsights
{
    public static class ApplicationInsightsInitialize
    {
        public static void AddTelemetry(this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            ApplicationInsights option = configuration.GetSection(nameof(ApplicationInsights)).Get<ApplicationInsights>();
            serviceCollection.AddSingleton(option);
            serviceCollection.AddApplicationInsightsTelemetry(option.InstrumentationKey);
            serviceCollection.AddSingleton(typeof(ITraceHelper), typeof(TraceHelper));
        }
    }
}
