﻿using Microsoft.ApplicationInsights;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Telemetry.AppInsights
{
    public class TraceHelper : ITraceHelper
    {
        private readonly TelemetryClient _telemetry;

        public TraceHelper(TelemetryClient telemetry)
        {
            _telemetry = telemetry;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="mensaje"></param>
        /// <param name="parameters"></param>
        public void TraceInfo(TraceInfoTypes type, string mensaje, IDictionary<string, object> parameters)
        {
            TraceRegister(type, mensaje, parameters);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="method"></param>
        /// <param name="parameters"></param>
        public void TraceInfo(string method, IDictionary<string, object> parameters)
        {
            TraceRegister(TraceInfoTypes.Information, method, parameters);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mensaje"></param>
        public void TraceInfo(string mensaje)
        {
            TraceRegister(TraceInfoTypes.Information, mensaje, new Dictionary<string, object>());
        }

        private void TraceRegister(TraceInfoTypes type, string method, IDictionary<string, object> parameters)
        {
            Task.Run(() =>
            {
                var sb = new StringBuilder(method);

                foreach (var parameter in parameters)
                {
                    sb.Append(" ");
                    if (parameter.Value == null)
                        sb.AppendFormat("[{0}=]", parameter.Key);
                    else if (parameter.Value is IEnumerable)
                    {
                        sb.AppendFormat("[{0}=", parameter.Key);

                        foreach (object v in parameter.Value as IEnumerable)
                            sb.Append(v);

                        sb.Append("]");
                    }
                    else
                    {
                        if (parameter.Value is Exception ex)
                        {
                            sb.AppendFormat("[{0}={1}]", parameter.Key, ex.ToString());
                        }
                        else
                        {
                            sb.AppendFormat("[{0}={1}]", parameter.Key, parameter.Value);
                        }
                    }
                }

                switch (type)
                {
                    case TraceInfoTypes.Error:
                        _telemetry.TrackException(new Exception(sb.ToString()));
                        break;

                    case TraceInfoTypes.Warning:
                        _telemetry.TrackTrace(sb.ToString());
                        break;

                    case TraceInfoTypes.Information:
                        _telemetry.TrackEvent(sb.ToString());
                        break;
                }
            });
        }
    }
}
