﻿namespace Telemetry.AppInsights
{
    public enum TraceInfoTypes
    {
        Information,
        Warning,
        Error,
    }
}
