using System.Collections.Generic;
using System.Threading.Tasks;
using Data.Contratos;
using Microsoft.EntityFrameworkCore;
using Models;

namespace Data.Repositorios
{
    public class RepositorioJugadores : IRepositorioGenerico<Jugador>
    {
        private readonly TorneoDbContext _contexto;
        private DbSet<Jugador> _dbSet;

        public RepositorioJugadores(TorneoDbContext contexto)
        {
            _contexto = contexto;
            _dbSet = _contexto.Set<Jugador>();
        }

        public Task<bool> Actualizar(Jugador entity)
        {
            throw new System.NotImplementedException();
        }

        public Task<Jugador> Agregar(Jugador entity)
        {
            throw new System.NotImplementedException();
        }

        public Task<bool> Eliminar(int id)
        {
            throw new System.NotImplementedException();
        }

        public async Task<Jugador> ObtenerAsync(int id)
        {
            return await _dbSet.SingleOrDefaultAsync(c => c.jug_id == id);
        }

        public async Task<IEnumerable<Jugador>> ObtenerTodosAsync()
        {
            return await _dbSet.ToListAsync();
        }
    }
}