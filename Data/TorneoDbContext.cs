using Data.Configuracion;
using Microsoft.EntityFrameworkCore;
using Models;

namespace Data
{
    public partial class TorneoDbContext : DbContext
    {
        public TorneoDbContext()
        {

        }

        public TorneoDbContext(DbContextOptions<TorneoDbContext> options) : base (options)
        {

        }

        public virtual DbSet<Jugador> Jugadores { get; set;}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new JugadorCfg());
            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}