using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Models;

namespace Data.Configuracion
{
    public class JugadorCfg : IEntityTypeConfiguration<Jugador>
    {
        public void Configure(EntityTypeBuilder<Jugador> entity)
        {
            entity.ToTable("jugadores", "torneos");

            entity.HasKey(e => e.jug_id);
            entity.Property(e => e.jug_dni);

            entity.Property(e => e.jug_nombre);
            entity.Property(e => e.jug_apellido);

        }
    }
}