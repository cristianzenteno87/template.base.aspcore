﻿using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;


namespace WebAPI.Validation
{
    public static class ValidatorExtension
    {
        public static void AddValidationExtension(this IServiceCollection serviceCollection)
        {
            serviceCollection.TryAddEnumerable(ServiceDescriptor.Transient<IApiDescriptionProvider, DefaultApiDescriptionProvider>());
            serviceCollection.AddMvcCore(config => config.Filters.Add(typeof(ModelStateExtensions)))
                .ConfigureApiBehaviorOptions(option => {
                    option.SuppressModelStateInvalidFilter = true;
                    option.SuppressMapClientErrors = true;
                })
                .AddJsonOptions(options => { options.JsonSerializerOptions.WriteIndented = true; })
                .AddApiExplorer()
                .AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<JugadorRequestDtoValidator>());
        }
    }
}
