﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Dtos;

namespace WebAPI.Validation
{
    public class JugadorRequestDtoValidator : AbstractValidator<JugadorRequestDto>
    {
        public JugadorRequestDtoValidator() 
        {
            RuleFor(x => x.nombre).NotEmpty().WithMessage("nombre es obligatorio");

        }

    }
}
