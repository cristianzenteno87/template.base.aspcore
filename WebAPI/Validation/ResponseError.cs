﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using System.Net;

namespace WebAPI.Validation
{
    public class ResponseError
    {
        public int Status { get; set; }
        public string Message { get; set; }
        public string DeveloperMessage { get; set; }
        public string TraceId { get; set; }
        public string[] ValidationErrors { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="status"></param>
        /// <param name="message"></param>
        /// <param name="validationErrors"></param>
        /// <returns></returns>
        public static ResponseError CreateHttpValidationError(HttpStatusCode status, string message, string[] validationErrors)
        {
            var httpError = CreateDefaultHttpError(status, message);
            httpError.ValidationErrors = validationErrors;
            return httpError;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="environment"></param>
        /// <param name="status"></param>
        /// <param name="message"></param>
        /// <param name="developerMessage"></param>
        /// <param name="traceId"></param>
        /// <returns></returns>
        public static ResponseError Create(IWebHostEnvironment environment, HttpStatusCode status, string message, string developerMessage, string traceId)
        {
            if (environment is null)
            {
                throw new System.ArgumentNullException(nameof(environment));
            }
            ResponseError httpError = CreateDefaultHttpError(status, message);
            httpError.TraceId = traceId;
            if (environment.IsDevelopment())
            {
                httpError.DeveloperMessage = developerMessage;
            }
            return httpError;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="status"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public static ResponseError CreateDefaultHttpError(HttpStatusCode status, string message)
        {
            var httpError = new ResponseError
            {
                Status = (int)status,
                Message = message
            };

            return httpError;
        }
    }
}
