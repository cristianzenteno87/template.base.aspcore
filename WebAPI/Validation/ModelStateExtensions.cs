﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Linq;
using System.Net;


namespace WebAPI.Validation
{
    public class ModelStateExtensions : ActionFilterAttribute
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                context.Result = GenerateValidationMessage(context.ModelState);
            }
            else
            {
                return;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public BadRequestObjectResult ManualValidation(ControllerContext context)
        {
            if (!context.ModelState.IsValid)
            {
                return GenerateValidationMessage(context.ModelState);
            }
            else
            {
                return null;
            }
        }

        private BadRequestObjectResult GenerateValidationMessage(ModelStateDictionary modelState)
        {
            string[] validationErrors = modelState
                ?.Keys
                .SelectMany(it => modelState[it].Errors)
                ?.Select(ex => ex.ErrorMessage)
                ?.ToArray();

            ResponseError error = ResponseError.CreateHttpValidationError(
                status: HttpStatusCode.BadRequest,
                message: "Validation errors found",
                validationErrors: validationErrors);

            return new BadRequestObjectResult(error);
        }
    }
}
