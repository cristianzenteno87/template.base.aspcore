using AutoMapper;
using Dtos;
using Models;

namespace WebAPI.Profiles
{
    public class TorneoProfile : Profile
    {
        public TorneoProfile()
        {
            CreateMap<Jugador, JugadorResponseDto>().ReverseMap();
        }
    }
}