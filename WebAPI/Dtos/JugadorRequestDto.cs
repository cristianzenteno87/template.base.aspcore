﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Dtos
{
    public class JugadorRequestDto
    {
        /// <summary>Nombre Jugador</summary> 
        /// <example>Juan</example>
        public string nombre { get; set; }

    }
}
