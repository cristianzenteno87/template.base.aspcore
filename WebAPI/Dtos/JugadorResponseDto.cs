namespace Dtos
{
    public class JugadorResponseDto
    {
        public int jug_id { get; set; }
        public int jug_dni { get; set; }        
        public string jug_nombre { get; set; }
        public string jug_apellido { get; set; }
        public string jug_apodo { get; set; }
        public string jug_estado { get; set; }        

    }
}