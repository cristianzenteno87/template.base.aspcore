using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Threading.Tasks;
using AutoMapper;
using Data.Contratos;
using Dtos;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Models;
using Telemetry.AppInsights;
using WebAPI.Dtos;
using WebAPI.Validation;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JugadoresController : ControllerBase
    {
        private IRepositorioGenerico<Jugador> _jugadorRepositorio;
        private readonly IMapper _mapper;
        private readonly ITraceHelper _traceHelper;


        public JugadoresController ( IRepositorioGenerico<Jugador> jugadorRepositorio, IMapper mapper, ITraceHelper traceHelper)
        {
            _jugadorRepositorio = jugadorRepositorio;
            _mapper = mapper;
            _traceHelper = traceHelper;
        }
        
        //// GET: api/jugadores
        /// <summary>
        /// Get players
        /// </summary>
        [HttpGet]
        [ProducesResponseType(typeof(JugadorResponseDto), StatusCodes.Status200OK )]
        public async Task<ActionResult<IEnumerable<JugadorResponseDto>>> Get()
        {
            try
            {
                var jugadores = await _jugadorRepositorio.ObtenerTodosAsync();
                return Ok( _mapper.Map<List<JugadorResponseDto>>(jugadores) );
                
            }
            catch (Exception ex)
            {
                _traceHelper.TraceInfo(TraceInfoTypes.Error, nameof(JugadoresController), new Dictionary<string, object>
                {
                    [nameof(ex)] = ex
                });

                return StatusCode(500, ex.Message);
            }
        }
        //// POST: api/jugadores
        /// <summary>
        /// Get string
        /// </summary>
        /// <param name="request"></param> 
        [HttpPost]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ResponseError), StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<string>> GetValue(JugadorRequestDto request)
        {
            try
            {
                _traceHelper.TraceInfo(nameof(JugadoresController), new Dictionary<string, object>
                {
                    [nameof(JugadorRequestDto)] = JsonSerializer.Serialize(request)
                });

                return Ok("Hola " + request.nombre);

            }
            catch (Exception ex)
            {
                _traceHelper.TraceInfo(TraceInfoTypes.Error, nameof(JugadoresController), new Dictionary<string, object>
                {
                    [nameof(ex)] = ex
                });
                return StatusCode(500, ex.Message);
            }
        }
    }
}